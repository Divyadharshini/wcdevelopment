# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-04-19 10:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems', '0009_problem_mobile_number'),
    ]

    operations = [
        migrations.RenameField(
            model_name='problem',
            old_name='mobile_number',
            new_name='email',
        ),
        migrations.AddField(
            model_name='problem',
            name='is_valid',
            field=models.BooleanField(default=False),
        ),
    ]
