# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-03-17 07:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='problem',
            name='hash_key',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
