# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-04-19 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems', '0008_auto_20180419_1339'),
    ]

    operations = [
        migrations.AddField(
            model_name='problem',
            name='mobile_number',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
