from datetime import timedelta
from rest_framework import serializers

from problems.models import District,State, Problem


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = '__all__'

class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'


class ProblemSerializer(serializers.ModelSerializer):
    state_name = serializers.ReadOnlyField()
    district_name = serializers.ReadOnlyField()
    category = serializers.CharField(source='get_category_display')
    class Meta:
        model = Problem
        fields = '__all__'

