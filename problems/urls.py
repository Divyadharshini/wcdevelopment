"""wcdevelopment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from problems import views
from problems.views import ApiLogin

urlpatterns = [
    url(r'^register/$', views.problem_registration, name='problem_registration'),
    url(r'^status/$', views.check_status, name='check_status'),
    url(r'^districts/$', views.DistrictList.as_view()),
    url(r'^states/$', views.StateList.as_view()),
    url(r'^apiregistrer/$', views.APIProblemRegistration.as_view()),
    url(r'^guidelines/$', views.guidelines_page, name="guidelines_page"),
    url(r'^legislation/women/$', views.women_related_legislation, name="legislation_women"),
    url(r'^legislation/child/$', views.child_related_legislation, name="legislation_child"),
    url(r'^legislation/other/$', views.other_related_legislation, name="legislation_other"),
    url(r'^about/icds$', views.about_icds, name="about_icds"),
    url(r'^slider/upload_image/$', views.upload_slider_image, name="upload_image"),
    url(r'^add/generate_key/$', views.generate_creation_key, name="add_user"),
    url(r'^enroll/', views.add_user, name="enroll_user"),
    url(r'^user_auto_complete/$', views.UserHeadsAutoComplete.as_view(), name='user_auto_complete'),
    url(r'^assign_portfolio/$', views.assign_portfolio, name='assign_portfolio'),
    url(r'^problems/education/$', views.education_problems, name='education_problems'),
    url(r'^problems/electricity/$', views.electricity_problems, name='electricity_problems'),
    url(r'^problems/food/$', views.food_problems, name='food_problems'),
    url(r'^problems/clothing/$', views.clothing_problems, name='clothing_problems'),
    url(r'^problems/finance/$', views.finance_problems, name='finance_problems'),
    url(r'^problems/hospital/$', views.hospital_problems, name='hospital_problems'),
    url(r'^problems/transport/$', views.transport_problems, name='transport_problems'),
    url(r'^problems/maintanence/$', views.maintanence_problems, name='maintanence_problems'),
    url(r'^problems/cleanliness/$', views.cleanliness_problems, name='cleanliness_problems'),
    url(r'^problems/change_status/([0-9]+)/([0-9]+)/$', views.change_status, name='change_status'),
    url(r'^problems/check_status_api/$', views.StatusAPI.as_view(), name='check_status_api'),
    url(r'^api/login/$', ApiLogin.as_view(),name="api_login"),
    url(r'^change_validity/([0-9]+)/$', views.change_validity,name="change_validity"),

]
