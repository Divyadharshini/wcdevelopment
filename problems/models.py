# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class State(models.Model):
    name = models.CharField(max_length=255)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class District(models.Model):
    name = models.CharField(max_length=255)
    status = models.BooleanField(default=True)
    state = models.ForeignKey(State,null=True,blank=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Problem(models.Model):
    EDUCATION = 0
    ELECTRICITY = 1
    HOSPITAL = 2
    FOOD = 3
    CLEANLINESS = 4
    CLOTHING = 5
    TRANSPORT = 6
    FINANCE = 7
    MAINTENANCE = 8
    ZERO_TO_SEVEN = 0
    EIGHT_TO_FIFTEEN = 1
    SIXTEEN_TO_TWENTYTWO = 2
    TWENTYTHREE_TO_THIRTY = 3
    ABOVE_30 = 4
    SEEN = 1
    SUBMITTED = 0
    PROGRESS = 2
    COMPLETED = 3
    STATUS_CHOICES = ((SUBMITTED,'Submitted'),(SEEN,'Seen'),(PROGRESS,'Progress'),(COMPLETED,'Completed'))
    CATEGORY_CHOICES = ((EDUCATION, "Education"), (ELECTRICITY, 'Electricity'), (HOSPITAL, 'Hospital'), (FOOD, 'Food'),
                        (CLEANLINESS, 'Cleanliness'), (CLOTHING, 'Clothing'), (TRANSPORT, 'Transport'),
                        (FINANCE, 'Finance'), (MAINTENANCE, 'Maintenance'))

    AGE_CHOICES = ((ZERO_TO_SEVEN, '0-7'), (EIGHT_TO_FIFTEEN, '8-15'), (SIXTEEN_TO_TWENTYTWO, '16-22'),
                   (TWENTYTHREE_TO_THIRTY, '23-30'),(ABOVE_30,'Above 30'))

    category = models.IntegerField(choices=CATEGORY_CHOICES, default=EDUCATION)
    state = models.ForeignKey(State,related_name='problem_state',null=True)
    district = models.ForeignKey(District,related_name='problem_district',null=True)
    anganwadi_centres = models.CharField(max_length=255)
    age = models.IntegerField(choices=AGE_CHOICES,default=ZERO_TO_SEVEN)
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='problem_images/',null=True,blank=True)
    description = models.TextField(max_length=2000)
    hash_key = models.CharField(max_length=255,null=True,blank=True)
    status = models.IntegerField(choices=STATUS_CHOICES,default=SUBMITTED)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    email = models.EmailField(null=False)
    is_valid_problem = models.BooleanField(blank=True,default=False)

    def __unicode__(self):
        return '%s' % (self.get_category_display)

    @property
    def district_name(self):
        return self.district.name

    @property
    def state_name(self):
        return self.state.name


class SliderImage(models.Model):
    image = models.ImageField()
    status = models.BooleanField(default=True)

class CreationKey(models.Model):
    key = models.CharField(max_length=2500,null=True,blank=True)
    status = models.BooleanField(default=True)