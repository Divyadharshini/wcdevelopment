# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User, AnonymousUser, Group
from django.shortcuts import render, redirect

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from problems.forms import ProblemRegistrationForm, UploadImage, AddUser, GenerationKeyForm, AssignPortfolioForm
from problems.models import Problem, District, State, CreationKey
from problems.serializers import DistrictSerializer, StateSerializer, ProblemSerializer
from problems.templatetags.admin_menu import is_admin


def problem_registration(request):
    hash_key = None
    forms = ProblemRegistrationForm()
    if request.method == "POST":
        forms = ProblemRegistrationForm(request.POST, request.FILES)
        if forms.is_valid():
            problem = forms.save()
            messages.add_message(request, messages.SUCCESS, "Registered Successfully!  " + str(problem.hash_key))
            forms = ProblemRegistrationForm()
            hash_key = str(problem.hash_key)
    return render(request, 'problems/register.html', {'forms': forms,'hash_key_value':hash_key})


def check_status(request):
    problem = None
    if request.method == "POST":
        key = request.POST['key']

        try:
            problem = Problem.objects.get(hash_key=key,is_valid_problem=True)

        except:
            messages.add_message(request, messages.ERROR, "No such registration occured!")

    return render(request, 'problems/check_status.html', {'problem': problem})


class ApiLogin(APIView):
    def post(self, request):
        data = request.data
        username = data['username']
        password = data['password']
        try:
            user = User.objects.get(username=username)
            if user.check_password(password):
                authority = 'user'
                groups = user.groups.all()
                if Group.objects.get(name="Admin Group") in groups:
                    return Response(
                    {'id': user.id, 'email': user.username, 'name': user.first_name,
                     'status': 'success','authority':'admin'})
                elif Group.objects.get(name="education_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'education head' })
                elif Group.objects.get(name="maintanence_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'maintanence head' })
                elif Group.objects.get(name="finance_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'finance head' })
                elif Group.objects.get(name="transport_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'transport head' })
                elif Group.objects.get(name="clothing_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success', 'authority':'clothing head'})
                elif Group.objects.get(name="cleanliness_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'cleanliness head' })
                elif Group.objects.get(name="food_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'food head' })
                elif Group.objects.get(name="hospital_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'hospital head' })
                elif Group.objects.get(name="electricity_group") in groups:
                    return Response(
                        {'id': user.id, 'email': user.username, 'name': user.first_name,
                         'status': 'success','authority':'electricity head' })

            else:
                return Response({'status': 'error', 'message': 'User/Password Incorrect!!'}, status=400)
        except User.DoesNotExist:
            return Response({'status': 'error', 'message': 'User/Password Incorrect!!'}, status=400)

    def get(self, request):
        if isinstance(request.user, AnonymousUser):
            return Response({'message': 'Username / Password is wrong!!', 'status': 'error'}, status=400)
        else:
            user = request.user
            return Response(
                {'user': 1})


class APIProblemRegistration(APIView):
    def post(self, request):

        data = request.data
        try:
            problem = Problem()
            problem.category = data['category']
            state = State.objects.get(id=int(data['state']))
            problem.state = state
            district = District.objects.get(id=int(data['district']))
            problem.district = district
            problem.anganwadi_centres = data['anganwadi_centres']
            problem.age = data['age']
            problem.title = data['title']
            problem.image = data['image']
            problem.description = data['description']
            name = str(problem.title)[:4]
            hash_val = str(hash(datetime.now()))[:3]
            problem.hash_key = name + hash_val
            problem.save()
            return Response(
                {'status': 'success', 'message': 'Registration Successful!!', 'unique_key': problem.hash_key})
        except Exception as e:
            return Response({'status': e, 'message': 'Registration is not possible now!!'}, status=404)


class DistrictList(APIView):
    serializer_class = DistrictSerializer

    def get(self, request):
        districts = District.objects.filter(status=True)
        serializer = self.serializer_class(districts, many=True)
        return Response(serializer.data)


class StateList(APIView):
    serializer_class = StateSerializer

    def get(self, request):
        states = State.objects.filter(status=True)
        serializer = self.serializer_class(states, many=True)
        return Response(serializer.data)


def guidelines_page(request):
    return render(request, 'problems/guidelines.html')


def women_related_legislation(request):
    return render(request, 'problems/women_related_legislation.html')


def child_related_legislation(request):
    return render(request, 'problems/child_related_legislation.html')


def other_related_legislation(request):
    return render(request, 'problems/other_legislation.html')


def about_icds(request):
    return render(request, 'problems/about_icds.html')


@user_passes_test(is_admin)
def upload_slider_image(request):
    form = UploadImage()
    if request.method == "POST":
        form = UploadImage(request.POST, request.FILES)
        print(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Uploaded Image Successfully!')
            return redirect('upload_image')
    return render(request, 'problems/upload_image.html', {'forms': form})


def add_user(request):
    akey = request.GET.get('akey')
    print(akey)
    if not CreationKey.objects.filter(status=True, key=akey).exists():
        return render(request, 'problems/error.html')
    forms = AddUser()
    if request.method == "POST":
        forms = AddUser(request.POST)
        if forms.is_valid():
            user = forms.save()
            key = CreationKey.objects.get(key=akey, status=True)
            key.status = False
            key.save()
            messages.add_message(request, messages.SUCCESS, "User added successfully!")
    return render(request, 'problems/add_user.html', {'forms': forms})


@user_passes_test(is_admin)
def generate_creation_key(request):
    forms = GenerationKeyForm()
    if request.method == "POST":
        forms = GenerationKeyForm(request.POST)
        if forms.is_valid():
            creation_key = forms.save()
            return render(request, 'problems/generate_user.html', {'forms': forms, 'creation_key': creation_key})
    return render(request, 'problems/generate_user.html', {'forms': forms})


class UserHeadsAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        query_set = User.objects.all()
        if self.q:
            query_set = query_set.filter(first_name__icontains=self.q)
        return query_set


def assign_portfolio(request):
    forms = AssignPortfolioForm()
    if request.method == "POST":
        forms = AssignPortfolioForm(request.POST)
        if forms.is_valid():
            assign = forms.save()
            messages.add_message(request, messages.SUCCESS, "Portfolio Assigned!!")
    return render(request, 'problems/assign_portfolio.html', {'forms': forms})


def education_problems(request):
    problems = Problem.objects.filter(category=Problem.EDUCATION,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.EDUCATION, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.EDUCATION, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.EDUCATION, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def clothing_problems(request):
    problems = Problem.objects.filter(category=Problem.CLOTHING,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.CLOTHING, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.CLOTHING, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.CLOTHING, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def finance_problems(request):
    problems = Problem.objects.filter(category=Problem.FINANCE,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.FINANCE, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.FINANCE, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.FINANCE, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def maintanence_problems(request):
    problems = Problem.objects.filter(category=Problem.MAINTENANCE,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.MAINTENANCE, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.MAINTENANCE, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.MAINTENANCE, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def cleanliness_problems(request):
    problems = Problem.objects.filter(category=Problem.CLEANLINESS,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.CLEANLINESS, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.CLEANLINESS, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.CLEANLINESS, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def hospital_problems(request):
    problems = Problem.objects.filter(category=Problem.HOSPITAL,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.HOSPITAL, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.HOSPITAL, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.HOSPITAL, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def transport_problems(request):
    problems = Problem.objects.filter(category=Problem.TRANSPORT,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.TRANSPORT, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.TRANSPORT, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.TRANSPORT, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def food_problems(request):
    problems = Problem.objects.filter(category=Problem.FOOD,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.FOOD, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.FOOD, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.FOOD, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def electricity_problems(request):
    problems = Problem.objects.filter(category=Problem.ELECTRICITY,is_valid_problem=True).order_by('-created_at')
    seen_count = Problem.objects.filter(category=Problem.ELECTRICITY, status=Problem.SEEN,is_valid_problem=True).count()
    progressing_count = Problem.objects.filter(category=Problem.ELECTRICITY, status=Problem.PROGRESS,is_valid_problem=True).count()
    completed_count = Problem.objects.filter(category=Problem.ELECTRICITY, status=Problem.COMPLETED,is_valid_problem=True).count()
    return render(request, 'problems/categorised_problems.html',
                  {'problems': problems, 'seen_count': seen_count, 'progressing_count': progressing_count,
                   'completed_count': completed_count})


def change_status(request, id, status):
    user = request.user
    problem = Problem.objects.get(id=id)
    problem.status = status
    problem.save()
    messages.add_message(request, messages.SUCCESS, 'Changed status successfully!')
    if (user.groups.filter(name='cleanliness_group')).exists():
        return redirect('cleanliness_problems')
    elif (user.groups.filter(name='education_group')).exists():
        return redirect('education_problems')
    elif (user.groups.filter(name='electricity_group')).exists():
        return redirect('electricity_problems')
    elif (user.groups.filter(name='finance_group')).exists():
        return redirect('finance_problems')
    elif (user.groups.filter(name='transport_group')).exists():
        return redirect('transport_problems')
    elif (user.groups.filter(name='maintanence_group')).exists():
        return redirect('maintanence_problems')
    elif (user.groups.filter(name='food_group')).exists():
        return redirect('food_problems')
    elif (user.groups.filter(name='hospital_group')).exists():
        return redirect('hospital_problems')
    elif (user.groups.filter(name='clothing_group')).exists():
        return redirect('clothing_problems')



class StatusAPI(APIView):
    serializer_class = ProblemSerializer
    def post(self, request):
        data = request.POST
        hash_key = data.get('hash_key')
        print(hash_key)
        try:
            problem = Problem.objects.get(hash_key=hash_key,is_valid_problem=True)
            serializer = self.serializer_class(problem)
            return Response(serializer.data)
        except User.DoesNotExist:
            return Response({'status': 'error', 'message': 'User/Password Incorrect!!'}, status=400)

    def get(self, request):
        return Response({'status':'error','message':'GET Request Not Allowed'})



def change_validity(request,id):
    problem = Problem.objects.get(id=id)
    problem.is_valid_problem = True
    problem.save()
    return redirect('home')