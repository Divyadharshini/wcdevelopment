import datetime

from django import template
from django.urls import reverse

register = template.Library()


@register.filter(name='is_admin')
def is_admin(user):
    return (user.groups.filter(name='Developer Group') or user.groups.filter(name="Admin Group"))

@register.filter(name='is_electricity_head')
def is_electricity(user):
    return (user.groups.filter(name='electricity_group')).exists()

@register.filter(name='is_education_head')
def is_education_head(user):
    return (user.groups.filter(name='education_group')).exists()

@register.filter(name='is_finance_head')
def is_finance_head(user):
    return (user.groups.filter(name='finance_group')).exists()

@register.filter(name='is_clothing_head')
def is_clothing_head(user):
    return (user.groups.filter(name='clothing_group')).exists()

@register.filter(name='is_food_head')
def is_food_head(user):
    return (user.groups.filter(name='food_group')).exists()

@register.filter(name='is_hospital_head')
def is_hospital_head(user):
    return (user.groups.filter(name='hospital_group')).exists()

@register.filter(name='is_maintanence_head')
def is_maintanence_head(user):
    return (user.groups.filter(name='maintanence_group')).exists()

@register.filter(name='is_transport_head')
def is_transport_head(user):
    return (user.groups.filter(name='transport_group')).exists()

@register.filter(name='is_cleanliness_head')
def is_cleanliness_head(user):
    return (user.groups.filter(name='cleanliness_group')).exists()

