from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from datetime import datetime

from dal import autocomplete
from django import forms
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.template.loader import render_to_string

from problems.models import Problem, SliderImage, CreationKey


class ProblemRegistrationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProblemRegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))

    class Meta:
        model = Problem
        exclude = ['hash_key', 'status','is_valid_problem']


    def save(self, commit=True):
        problem = self.instance.save()
        name = str(self.instance.title)[:4]
        hash_val = str(hash(datetime.now()))[:3]
        self.instance.hash_key = name + hash_val
        self.instance.save()
        if self.instance.category == Problem.ELECTRICITY:
            to = list(Group.objects.get(name='electricity_group').user_set.all())
        elif self.instance.category == Problem.EDUCATION:
            to = list(Group.objects.get(name='education_group').user_set.all())
        elif self.instance.category == Problem.HOSPITAL:
            to = list(Group.objects.get(name='hospital_group').user_set.all())
        elif self.instance.category == Problem.FOOD:
            to = list(Group.objects.get(name='food_group').user_set.all())
        elif self.instance.category == Problem.CLEANLINESS:
            to = list(Group.objects.get(name='cleanliness_group').user_set.all())
        elif self.instance.category == Problem.CLOTHING:
            to = list(Group.objects.get(name='clothing_group').user_set.all())
        elif self.instance.category == Problem.TRANSPORT:
            to = list(Group.objects.get(name='transport_group').user_set.all())
        elif self.instance.category == Problem.MAINTENANCE:
            to = list(Group.objects.get(name='maintanence_group').user_set.all())
        else:
            to = list(Group.objects.get(name='finance_group').user_set.all())

        try:
            html_message = render_to_string('problems/SD Template.html', {'category': self.instance.get_category_display(),
                                                                      'title': self.instance.title,'url':self.instance.image.url,})
            html_message1 = render_to_string('problems/SD Template1.html',
                                             {'category': self.instance.get_category_display(),
                                              'title': self.instance.title, 'hash_key': self.instance.hash_key,'url':self.instance.image.url,'id':self.instance.id})
        except:
            html_message = render_to_string('problems/SD Template.html',
                                            {'category': self.instance.get_category_display(),
                                             'title': self.instance.title})

            html_message1 = render_to_string('problems/SD Template1.html',
                                            {'category': self.instance.get_category_display(),
                                             'title': self.instance.title, 'hash_key':self.instance.hash_key,'id':self.instance.id})

        send_mail('Ministry of Women and Child Development', 'Complaint has been recorded!', "MOWACD",
                  to, fail_silently=False, html_message=html_message)

        send_mail('Ministry of Women and Child Development', 'Complaint has been recorded!', "MOWACD",
                  [self.instance.email], fail_silently=False, html_message=html_message1)

        return self.instance


class UploadImage(forms.ModelForm):
    class Meta:
        model = SliderImage
        fields = ['image']

    def __init__(self, *args, **kwargs):
        super(UploadImage, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))


class AddUser(forms.Form):
    first_name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(AddUser, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Register"))

    def is_valid(self):
        email = self.__getitem__('email').value()

        password = self.__getitem__('password').value()
        confirm_password = self.__getitem__('confirm_password').value()
        is_valid = True
        try:
            email = User.objects.get(email=email)
            self.add_error('email', 'The User already exists!')
            is_valid = False
        except:
            pass
        try:
            if password == confirm_password:
                pass
            else:
                self.add_error('password', 'The passwords do not match!')
                is_valid = False
        except:
            pass
        return is_valid

    def save(self, commit=True):
        email = self.__getitem__('email').value()
        password = self.__getitem__('password').value()
        confirm_password = self.__getitem__('confirm_password').value()
        first_name = self.__getitem__('first_name').value()
        user = User.objects.create_user(email=email, first_name=first_name, password=password, username=email)
        return user


class GenerationKeyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(GenerationKeyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Generate Key!"))

    def save(self):
        hash_val = str(hash(datetime.now()))
        CreationKey.objects.create(key=hash_val)
        return hash_val


class AssignPortfolioForm(forms.Form):
    EDUCATION = 0
    ELECTRICITY = 1
    HOSPITAL = 2
    FOOD = 3
    CLEANLINESS = 4
    CLOTHING = 5
    TRANSPORT = 6
    FINANCE = 7
    MAINTANENCE = 8

    user = forms.ModelChoiceField(
        queryset=User.objects.all(),
        empty_label="Error Fetching Staff",
        widget=autocomplete.ModelSelect2(url='user_auto_complete')
    )
    CATEGORY_CHOICES = ((EDUCATION, "Education"), (ELECTRICITY, 'Electricity'), (HOSPITAL, 'Hospital'), (FOOD, 'Food'),
                        (CLEANLINESS, 'Cleanliness'), (CLOTHING, 'Clothing'), (TRANSPORT, 'Transport'),
                        (FINANCE, 'Finance'), (MAINTANENCE, 'Maintenance'))
    portfolio = forms.ChoiceField(choices=CATEGORY_CHOICES, initial=0)

    def __init__(self, *args, **kwargs):
        super(AssignPortfolioForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))

    def save(self, commit=True):
        data = self.cleaned_data
        user = data['user']
        port = data['portfolio']
        if int(data['portfolio']) == AssignPortfolioForm.EDUCATION:
            group = Group.objects.get(name='education_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.ELECTRICITY:
            group = Group.objects.get(name='electricity_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.HOSPITAL:
            group = Group.objects.get(name='hospital_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.FOOD:
            group = Group.objects.get(name='food_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.CLEANLINESS:
            group = Group.objects.get(name='cleanliness_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.CLOTHING:
            group = Group.objects.get(name='clothing_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.TRANSPORT:
            group = Group.objects.get(name='transport_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.FINANCE:
            group = Group.objects.get(name='finance_group')
            user.groups.add(group)
        elif int(data['portfolio']) == AssignPortfolioForm.MAINTANENCE:
            group = Group.objects.get(name='maintanence_group')
            user.groups.add(group)
        user.save()
