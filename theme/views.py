# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from problems.models import SliderImage


def home(request):
    images = SliderImage.objects.filter(status=True)
    return render(request,'theme/landing_page.html',{'images':images})

def dashboard(request):
    return render(request,'problems/dashboard.html')