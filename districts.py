from problems.models import State, District

file = open('districts.csv')

for i in file:
    line = i.split(',')
    try:
        districts = District.objects.filter(name=line[1])
        if districts:
            pass
        else:
            state_name = str(line[0]).upper()
            print(state_name)
            state = State.objects.get(name=state_name)
            District.objects.create(name=line[1],status=True,state= state)
            print('Stored! '+line[1])
    except Exception as e:
        print(e)
        break

